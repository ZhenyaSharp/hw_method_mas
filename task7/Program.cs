﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace procAndFunction
{
    class Program
    {
        static int[] CreateMas()
        {
            Console.Write("введите размер массива: ");
            int n = int.Parse(Console.ReadLine());

            return new int[n];
        }
        static void FillMasRnd(int[] mas)
        {
            Random rnd = new Random();
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = rnd.Next(0, 100);
            }
        }

        static void PrintMas(int[] mas)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                Console.Write($"{mas[i]} ");
            }
            Console.WriteLine();
        }

        static void Reverse(int[] mas)
        {
            int temp = 0;

            for (int i = 0; i < mas.Length-1; i++)
            {
                temp = mas[i];
                mas[i] = mas[mas.Length-i-1];
                mas[mas.Length - i-1] = temp;
            }
                for (int i = 0; i < mas.Length; i++)
                {
                    Console.Write($"{mas[i]} ");
                }
        }


    static void Main(string[] args)
    {
        int[] mas = CreateMas();
        FillMasRnd(mas);
        PrintMas(mas);
        Reverse(mas);


        Console.ReadKey();
    }
}
}
