﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace procAndFunction
{
    class Program
    {
        static int[] CreateMas()
        {
            Console.Write("введите размер массива: ");
            int n = int.Parse(Console.ReadLine());

            return new int[n];
        }
        static void FillMasRnd(int[] mas)
        {
            Random rnd = new Random();
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = rnd.Next(0, 100);
            }
        }

        static void PrintMas(int[] mas)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                Console.Write($"{mas[i]} ");
            }
            Console.WriteLine();
        }

        static int FindIndex(int[] mas, int k)
        {
            int KIsMis = -1;
            for (int i = 0; i < mas.Length; i++)
            {
                if (mas[i] == k)
                {
                    return i;
                }
            }
            return KIsMis;
        }
        static void Main(string[] args)
        {
            int result;
            int[] mas = CreateMas();
            FillMasRnd(mas);
            PrintMas(mas);
            result = FindIndex(mas);

            Console.WriteLine("Index = "+result);

            Console.ReadKey();
        }
    }
}
