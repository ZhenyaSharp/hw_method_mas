﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace procAndFunction
{
    class Program
    {
        static int[] CreateMas()
        {
            Console.Write("введите размер массива: ");
            int n = int.Parse(Console.ReadLine());

            return new int[n];
        }
        static void FillMasRnd(int[] mas)
        {
            Random rnd = new Random();
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = rnd.Next(0, 100);
            }
        }

        static void PrintMas(int[] mas)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                Console.Write($"{mas[i]} ");
            }
            Console.WriteLine();
        }

        static void MultElements(int[] mas)
        {
            Console.Write("введите во сколько раз увеличить: ");
            int k = int.Parse(Console.ReadLine());

            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] *= k;;
            }
            for (int i = 0; i < mas.Length; i++)
            {
                Console.Write($"{mas[i]} ");
            }
        }

        static void Main(string[] args)
        {
            int[] mas = CreateMas();
            FillMasRnd(mas);
            PrintMas(mas);
           MultElements(mas);


            Console.ReadKey();
        }
    }
}
