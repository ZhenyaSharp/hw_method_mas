﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace procAndFunction
{
    class Program
    {
        static int[] CreateMas()
        {
            Console.Write("input n: ");
            int n = int.Parse(Console.ReadLine());

            return new int[n];
        }
        static void FillMasRnd(int[] mas)
        {
            Random rnd = new Random();
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = rnd.Next(0, 100);
            }
        }

        static void PrintMas(int[] mas)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                Console.Write($"{mas[i]} ");
            }
            Console.WriteLine();
        }
        static int FindMax(int[] mas)
        {
            int max = mas[0];
            for (int i = 0; i < mas.Length; i++)
            {
                if (mas[i] > max)
                {
                    max = mas[i];
                }
            }
            return max;
        }
        static void Main(string[] args)
        {
            int max;
            int[] mas = CreateMas();
            FillMasRnd(mas);
            PrintMas(mas);
            max = FindMax(mas);

            Console.WriteLine(max);

            Console.ReadKey();
        }
    }
}
