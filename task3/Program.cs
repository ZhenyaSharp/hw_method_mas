﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace procAndFunction
{
    class Program
    {
        static int[] CreateMas()
        {
            Console.Write("input n: ");
            int n = int.Parse(Console.ReadLine());

            return new int[n];
        }
        static void FillMasRnd(int[] mas)
        {
            Random rnd = new Random();
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = rnd.Next(0, 100);
            }
        }

        static void PrintMas(int[] mas)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                Console.Write($"{mas[i]} ");
            }
            Console.WriteLine();
        }
        static int FindMin(int[] mas)
        {
            int min=mas[0];
            for (int i = 0; i < mas.Length; i++)
            {
                if (mas[i] < min)
                {
                    min = mas[i];
                }
            }
            return min;
        }
        static void Main(string[] args)
        {
            int min;
            int[] mas = CreateMas();
            FillMasRnd(mas);
            PrintMas(mas);
            min = FindMin(mas);

            Console.WriteLine(min);

            Console.ReadKey();
        }
    }
}
